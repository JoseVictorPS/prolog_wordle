%Esse arquivo contém as funções relacionadas com encontrar os acertos e erros

/*
regra que busca as letras de palpite que estão na posição correta
Gera 3 listas: 
    -uma só com os acertos
    -uma com a palavra certa sem letras que estão na posição correta
    -uma com o palpite tentado sem as letras que estão posição correta
Ex. Palavra = ['b','a','l','a','o'] Palpite = ['b','a','l','s','a']
Acertos = ['b','a','l','_','_']
PalavraSemAcertos = ['_','_','_','a','o']
PalpiteSemAcertos = ['_','_','_','s','a']
*/
acertos([],[],[],[],[]). 
acertos([LetraPalavra|RestoPalavra], [LetraPalpite|RestoPalpite], Acertos, PalavraSemAcertos, PalpiteSemAcertos) :- 
    %chama recursivamente ate chegar ao caso base e depois faz a verificação do fim para o começo
    acertos(RestoPalavra, RestoPalpite, Ac, P1, P2), 
    %verifica se a letra atual é um acerto e preenche as listas de acordo '_' indica vazio.
    ((LetraPalavra = LetraPalpite) -> 
        Acertos = [LetraPalavra|Ac],
        PalavraSemAcertos = ['_'|P1],
        PalpiteSemAcertos = ['_'|P2]; 
        Acertos = ['_'|Ac],
        PalavraSemAcertos = [LetraPalavra|P1],
        PalpiteSemAcertos = [LetraPalpite|P2]).

/*
essa regra utiliza os resultados da busca pelos acertos para encontrar os semi-acertos e os erros.
utilizando o exemplo anterior, essa regra iria gerar:
SemiAcertos = ['_','_','_','_','a']
Erros = ['_','_','_','s','_']
*/
semiAcertos(_,[],[],[]).
semiAcertos(PalavraSemAcertos, [LetraPalpite|PalpiteSemAcertos], SemiAcertos, Erros) :- 
    %se a letraPalpite for '_' siginifica que ela já é um acerto e, portanto, não deve ser verificada
    (   LetraPalpite \= '_' ->
        (   esta_na_lista(LetraPalpite, PalavraSemAcertos) -> 
                % se a letra não é um acerto mas está na palavra, então é um semi acerto
                SemiAcertos = [LetraPalpite|Semi],
                Erros = ['_'|Err],
                %atualiza a palavra para evitar considerar a mesma letra duas vezes como um semi-acerto
                substitui(LetraPalpite, PalavraSemAcertos, '_', PalavraAtualizada)
            ;   SemiAcertos = ['_'|Semi],
                Erros = [LetraPalpite|Err],
                PalavraAtualizada = PalavraSemAcertos
        )
        ;   SemiAcertos = ['_'|Semi],
            Erros = ['_'|Err],
            PalavraAtualizada = PalavraSemAcertos
    ),semiAcertos(PalavraAtualizada, PalpiteSemAcertos, Semi, Err).

%apenas organiza as duas regras anteriores
adivinha(Palavra,Tentativa) :-
    acertos(Palavra, Tentativa, Acertos, PalavraSemAcertos, PalpiteSemAcertos),
    semiAcertos(PalavraSemAcertos, PalpiteSemAcertos, SemiAcertos, Erros),
    write("Letras na posicao certa: "), write(Acertos), nl,
    write("Letras na posicao errada: "), write(SemiAcertos), nl,
    write("Letras que nao tem na palavra: "), write(Erros), nl.

    