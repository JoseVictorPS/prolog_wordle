:- consult('jogo').
:- consult('util').
:- consult('wordle').

%seleciona uma palavra de um determinado tamanho dos arquivos de dicionario
escolhe_palavra(Tamanho, Palavra) :-
    atom_concat('palavras/dicio', Tamanho, Prefixo),
    atom_concat(Prefixo, '.txt', NomeArquivo),
    open(NomeArquivo, read, Stream),
    le_arquivo(Stream, Linhas), %Busca todas as linhas do arquivo
    tamanho_lista(Linhas,Len),
    Len1 is Len - 1,
    random(0,Len1,Pos), 
    enesimo(Pos, Linhas, Palavra), %Busca a palavra em um índice aleatório
    close(Stream).

%Tenta ler o numero de letras até que o usuário digite um número válido
le_numero_de_letras(Numero) :-
    repeat,
        write("Escolha o tamanho da palavra(numero entre 4 e 7): "), nl,
        read(Numero),
        (
            Numero >= 4, Numero =< 7 ->
            true, ! %Se o número ja estiver dentro do range, faz o cut
        ;   fail %caso contrário, faz o backtracking para o repeat
        ).

main :-
    le_numero_de_letras(TamanhoPalavra),
    escolhe_palavra(TamanhoPalavra, PalavraRaw), !, %Evita backtracking na Stream já fechada do arquivo de palavras.
    %write("Palavra objetivo: "), write(PalavraRaw), nl, %Revela a palavra, apenas para debug.
    atom_chars(PalavraRaw, Palavra),
    gameloop(6, Palavra). % O jogador tem 6 vidas.