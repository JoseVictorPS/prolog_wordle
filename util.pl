%Este arquivo contém funções de regras auxiliares do programa

tamanho_lista([],0).
tamanho_lista([_|T],Tamanho) :-
    tamanho_lista(T,Tamanho1),
    Tamanho is Tamanho1 + 1.

igual([], []).
igual([H1|T1], [H2|T2]):-
    H1 = H2,
    igual(T1, T2).

escreve_palavra([]).
escreve_palavra([Letra|Palavra]) :-
    write(Letra),
    escreve_palavra(Palavra).

%retorna o elemento de uma lista que está na posição Pos.
enesimo(0, [H|_], H) :- !. %se pos chegou a 0 então é o elemento buscado
enesimo(Pos, [_|T], Res) :-
    Pos1 is Pos -1, %diminui pos e chama recursivamente
    enesimo(Pos1, T, Res).

tamanho_igual([], []).
tamanho_igual([_|T1], [_|T2]) :-
    tamanho_igual(T1,T2).

le_arquivo(Stream, []) :- at_end_of_stream(Stream).
le_arquivo(Stream, [H|T]) :-
    \+ at_end_of_stream(Stream),
    read(Stream, H),
    le_arquivo(Stream, T).

%substitui a primeira aparição de A na lista B por C e armazena em D.
substitui(A,B,C,D) :- select(A,B,C,D), !.

esta_na_lista(A, [A|_]).
esta_na_lista(A, [_|T]) :-
    esta_na_lista(A, T).