% Este arquivo tem as funções relacionadas diretamente à lógica de Game Loop

%Tenta ler o palpite até que o usuário digite uma palavra com a quantidade de caracteres válida
le_palpite(Palavra, Palpite) :-
    repeat,
        write("Tente um palpite(deve ter o número de letras informado): "), nl,
        read(PalpiteRaw),
        atom_chars(PalpiteRaw, Palpite),
        (
            tamanho_igual(Palavra,Palpite) ->
            true, ! 
        ;   fail 
        ).

ainda_vivo(Vidas) :- Vidas > 0.

verifica_vitoria(Palavra, Palpite) :- igual(Palavra, Palpite) -> write("Parabens, voce acertou!").

gameloop(0, Palavra) :- write("Game Over! A palavra era: "), escreve_palavra(Palavra), nl.
gameloop(Vidas, Palavra) :-
    ainda_vivo(Vidas),
    le_palpite(Palavra, Palpite),
    Vidas1 is Vidas - 1,
    \+ verifica_vitoria(Palavra, Palpite), %Segue o jogo, se não ganhou
    adivinha(Palavra, Palpite),
    write("Vidas restantes: "), write(Vidas1), nl,
    gameloop(Vidas1, Palavra).


